# Password Generator

This simple password generator generates passwords of 8 to 32 characters, including letters, numbers, and special symbols.

## Usage

To generate a password, you can run the `password_generator.py` script. Optionally, you can specify the length of the password using the `PASSWORD_LENGTH` environment variable.

### Example

```bash
# Generate a default password (length: 16)
python main.py


# Generate a password with a custom length (e.g., 12)
PASSWORD_LENGTH=12 python main.py