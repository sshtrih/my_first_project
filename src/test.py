from main import generate_password

import pytest

def test_generate_password_default_length():
    password = generate_password()
    assert 8 <= len(password) <= 32

def test_generate_password_custom_length():
    password = generate_password(12)
    assert len(password) == 12

def test_generate_password_invalid_length():
    with pytest.raises(ValueError):
        generate_password(5)